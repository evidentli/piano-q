from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='piano_q',
    packages=find_packages(exclude=["tests"]),
    version='0.1.0',
    description='Piano Queue',
    author='Michael Van Treeck',
    author_email='michael@evidentli.com',
    url='https://bitbucket.org/evidentli/piano-q',
    license='MIT',
    keywords=['piano', 'util', 'queue', 'redis', 'broker'],
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.0"
    ],
    install_requires=requirements,
)
