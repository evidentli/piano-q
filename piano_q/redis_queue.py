import traceback
from enum import Enum

from redis import Redis
from rq import Queue
from fakeredis import FakeStrictRedis

from piano_q.piano_queue import PianoQ, JobStatus


class _RedisJobStatus(Enum):
    queued = "queued"
    started = "started"
    finished = "finished"
    failed = "failed"


class RedisQ(PianoQ):

    def __init__(self, name, host, port, **kwargs):
        super(RedisQ, self).__init__(name, host, port, **kwargs)

    def _init_q(self, **kwargs):
        connection_kwargs = {}
        if self.host:
            connection_kwargs["host"] = self.host
        if self.port:
            connection_kwargs["port"] = self.port
        connection = Redis(**connection_kwargs)
        options = dict(default_timeout=self.default_timeout)
        kwargs.update(options)
        if self.testing:
            self.q = Queue(self.name, is_async=False, connection=FakeStrictRedis())
        else:
            self.q = Queue(self.name, connection=connection, **kwargs)

    def _fetch_job(self, job_id):
        try:
            return self.q.fetch_job(job_id)
        except Exception as e:
            print(traceback.format_exc(e))
            return None

    def empty_queue(self):
        try:
            self.q.empty()
        except Exception as e:
            print(traceback.format_exc(e))
            return False
        return True

    def delete_queue(self):
        try:
            self.q.delete(delete_jobs=True)
        except Exception as e:
            print(traceback.format_exc(e))
            return False
        return True

    def enqueue(self, function, job_id=None, *args, **kwargs):
        if job_id:
            kwargs["job_id"] = job_id
        kwargs["result_ttl"] = kwargs.get("timeout", self.default_result_expiry)
        try:
            job = self.q.enqueue(function, *args, **kwargs)
        except Exception as e:
            print(traceback.format_exc(e))
            return False
        return job.id if job else False

    def delete(self, job_id):
        job = self._fetch_job(job_id)
        if job:
            job.delete()

    def get_status(self, job_id):
        job = self._fetch_job(job_id)
        if job:
            status = job.get_status()
            if status == _RedisJobStatus.queued.value:
                return JobStatus.queued.value
            elif status == _RedisJobStatus.started.value:
                return JobStatus.started.value
            elif status == _RedisJobStatus.finished.value:
                return JobStatus.finished.value
            elif status == _RedisJobStatus.failed.value:
                return JobStatus.failed.value
        return None

    def is_pending(self, job_id):
        return self.get_status(job_id) in [JobStatus.started.value, JobStatus.queued.value]

    def is_complete(self, job_id):
        return self.get_status(job_id) == JobStatus.finished.value

    def get_result(self, job_id):
        job = self._fetch_job(job_id)
        if job:
            return job.result
        return None

    def get_jobs(self):
        jobs = []
        try:
            for job_id in self.q.get_job_ids():
                job = self.q.fetch_job(job_id)
                jobs.append({
                    "status": job.get_status(),
                    "args": job.args,
                })
        except Exception as e:
            print(traceback.format_exc(e))
        return jobs
