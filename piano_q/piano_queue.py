from future.utils import with_metaclass

from abc import abstractmethod, ABCMeta

from enum import Enum


class JobStatus(Enum):
    queued = "queued"
    started = "started"
    finished = "finished"
    failed = "failed"


class PianoQ(with_metaclass(ABCMeta, object)):

    q = None

    def __init__(self, name, host, port, default_timeout=300, default_result_expiry=500, testing=False, **kwargs):
        """
        Piano Queue Abstract Class
        :param name: name of queue
        :param host: broker/queue host name
        :param port: broker/queue port
        :param default_timeout: (optional, default=300) execution timeout, in seconds, for each job
        :param default_result_expiry: (optional, default=500) execution result expiry, in seconds, for each job
        :param testing: (optional, default=False) mock the queue (will work in synchronous mode)
        :param kwargs:
        """
        self.name = name
        self.host = host
        self.port = port
        self.default_timeout = default_timeout
        self.default_result_expiry = default_result_expiry
        self.testing = testing
        self._init_q(**kwargs)

    @abstractmethod
    def _init_q(self, **kwargs):
        """
        Initialise q
        :return:
        """
        pass

    @abstractmethod
    def empty_queue(self):
        """
        Empty the queue of all jobs
        :return:
        """
        pass

    @abstractmethod
    def delete_queue(self):
        """
        Delete the queue (as well as any jobs in the queue)
        :return:
        """
        pass

    @abstractmethod
    def enqueue(self, function, job_id=None, timeout=None, *args, **kwargs):
        """
        Add job to the queue
        :param function: function to execute
        :param job_id: (optional) unique job identifier
        :param timeout: (optional) execution timeout, in seconds, for this job
        :param args: arguments to pass to function when executed
        :param kwargs: keyword arguments to pass to function when executed
        :return: job_id if successful, False if job wasn't queued
        """
        pass

    @abstractmethod
    def delete(self, job_id):
        """
        Delete a job
        :param job_id: identifier of job to delete
        :return:
        """
        pass

    @abstractmethod
    def get_status(self, job_id):
        """
        Get status of job using given job_id
        :param job_id: identifier of job to retrieve status for
        :return: status (see JobStatus)
        """
        pass

    @abstractmethod
    def is_pending(self, job_id):
        """
        If job for given id is not yet complete (i.e. started or queued)
        :param job_id: identifier of job
        :return: bool
        """
        pass

    @abstractmethod
    def is_complete(self, job_id):
        """
        If job for given id is complete
        :param job_id: identifier of job
        :return: bool
        """
        pass

    @abstractmethod
    def get_result(self, job_id):
        """
        Get result of job function execution
        :param job_id: identifier of job
        :return:
        """
        pass

    @abstractmethod
    def get_jobs(self):
        """
        Get all jobs in the queue
        :return: list of jobs in queue
        """
        pass


from piano_q.redis_queue import RedisQ


def get_q(name, host=None, port=None, default_timeout=300, default_result_expiry=500, testing=False, **kwargs):
    """
    Get Piano Q object
    :param name: name of queue
    :param host: host name of piano q server
    :param port: host name of piano q port
    :param default_timeout: (optional, default=300) execution timeout, in seconds, for each job
    :param default_result_expiry: (optional, default=500) execution result expiry, in seconds, for each job
    :param testing: (optional, default=False) mock the queue (will work in synchronous mode)
    :param kwargs:
    :return: PianoQ
    """
    return RedisQ(name, host, port,
                  default_timeout=default_timeout,
                  default_result_expiry=default_result_expiry,
                  testing=testing,
                  **kwargs)
